package net.ihe.gazelle.hl7templates.packager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.common.util.ZipUtility;
import net.ihe.gazelle.tempgen.action.FileReadWrite;
import net.ihe.gazelle.tempgen.action.GenerationProperties;
import net.ihe.gazelle.tempgen.action.HL7TemplatesConverter;
import net.ihe.gazelle.tempgen.flatten.action.ProjectFlattenProc;
import net.ihe.gazelle.tempgen.valueset.action.ValueSetExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

public final class GOCPackager {
	
	private static final String CD_COMMAND = "cd \"";
	private static final String MODEL_PREFIX = "-model";
	private static Logger log = LoggerFactory.getLogger(GOCPackager.class);
	
	private GOCPackager() {}
	
	public static void main(String[] args) throws Exception {
		//String bbr = "/home/aboufahj/JP/art-decor-workspace/IHCPCCADD-20161024T093032-en-US-decor-compiled.xml";
		String bbr = "/tmp/ccda-20170601T110310-decor.xml";
		String folderOutput = "/home/aboufahj/workspaceTopcased/hl7templates-packager-jar/ccda21/";
		String folderInput = "/home/aboufahj/workspaceTopcased/hl7templates-packager-jar/src/main/resources/";
		String properties = "/home/aboufahj/workspaceTopcased/hl7templates-packager-jar/src/main/resources/conf.properties";
		String mavnExecPath ="mvn";
		String serviceName = "HL7 - C-CDA R2.1";
		packageBBRToGOCValidationTool(bbr, serviceName, folderOutput, folderInput, properties, true, mavnExecPath, true, "2.1", null);
	}

	public static void packageBBRToGOCValidationTool(String bbr, String serviceName, String folderOutput, String folderInput, 
			String propFileParam, Boolean notOverride, String mavnExecPath, Boolean ignoreTemplateIdRequirements, String versionLabel, 
			String validatorRootClass) throws IOException, InterruptedException, JAXBException {
		String propFile = propFileParam==null?"src/main/resources/conf.properties":propFileParam;
		// Creates output folder /!\ delete if already exists
		logSeparator("Creates output folder /!\\ delete if already exists");
		treateOutputFolder(folderOutput);
		// Creates checkout.sh in output folder and execute it. Some logs are available in jboss
		logSeparator("Creates checkout.sh for model in output folder and execute it. Some logs are available in jboss");
		checkoutNeededModels(folderOutput);
		// Overrides checkout.sh in output folder and execute it. Some logs are available in jboss
		logSeparator("Overrides checkout.sh for archetype in output folder and execute it. Some logs are available in jboss");
		checkoutArchetypes(folderOutput);
		// Replace path in modelsDefinition.xml with output folder
		logSeparator("Replace path in modelsDefinition.xml with output folder");
		UpdateModelsDefinition.updateModelDefinitions(notOverride, folderOutput);
		// Copy folderInput and ocl2java-1.1.4.jar in folderOutput
		logSeparator("Copy folderInput and ocl2java-1.1.4.jar in folderOutput");
		copyProjectModel(bbr, folderInput, folderOutput);
		// Generates the UML model for the bbr
		logSeparator("Generates the UML model for the bbr");
		generateNewModel(bbr, folderOutput, ignoreTemplateIdRequirements, versionLabel);
		// Copy project-templates/project-validator-jar in outPutFolder
		logSeparator("Copy project-templates/project-validator-jar in outPutFolder");
		copyProjectValidator(bbr, folderInput, folderOutput, serviceName, validatorRootClass);
		// Execute Maven plugin to generate the validator mbgen:genvalidator
		logSeparator("Execute Maven plugin to generate the validator mbgen:genvalidator");
		generateProjectValidator(bbr, folderOutput, mavnExecPath);
		// Execute Maven plugin to generate the CDA Template mbgen:gencdatree
		logSeparator("Execute Maven plugin to generate the CDA Template mbgen:gencdatree");
		generateProjectCDATemplatesTree(bbr, folderOutput, mavnExecPath);
		// Replace imports (with conf.properties from forges) and CommonOperationsStaticValidator class
		logSeparator("Replace imports (with conf.properties from forges) and CommonOperationsStaticValidator class");
		updateImportsForValidator(bbr, propFile, folderOutput);
		// Maven package the jar
		logSeparator("Maven package the jar");
		assembleProjectValidator (bbr, folderOutput, mavnExecPath);
		// Copy valresources from folderInput in the target/appAssembler/bin/resources of the produced jar
		logSeparator("Copy valresources from folderInput in the target/appAssembler/bin/resources of the produced jar");
		copyResourcesForBinAssembler(bbr, folderInput, folderOutput);
		// EXtract Value-sets from the BBR and put htem in appassembler/bin/resources
		logSeparator("Extract Value-sets from the BBR and put htem in appassembler/bin/resources");
		extractValueSets(bbr, folderOutput);
		// Execute Maven plugin to extract Documentation mbgen:genxmldoc
		logSeparator("Execute Maven plugin to extract Documentation mbgen:genxmldoc");
		extractDocumentation(bbr, folderOutput, mavnExecPath);
		// chmod777 on appassembler/bin/validate.sh
		logSeparator("chmod777 on appassembler/bin/validate.sh");
		updateCHMODValidateScript(bbr, folderOutput);
		// Copy appassembler and zip it
		logSeparator("Copy appassembler and zip it");
		copyAssembledValidationToolToOutput(bbr, folderOutput);
	}
	
	private static void extractDocumentation(String bbr, String folderOutput, String mavnExecPath) throws IOException, InterruptedException {
		executeMavenPluginTarget(bbr, folderOutput, mavnExecPath, "mbgen:genxmldoc");
	}
	
	private static void executeMavenPluginTarget(String bbr, String folderOutput, String mavnExecPath, String mavenTarget) throws IOException, InterruptedException {
		File file = new File(folderOutput);
		String projectName = extractProjectName(bbr);
		String execGen =  "#!/bin/bash\n" +
				CD_COMMAND + file.getAbsolutePath() + File.separator + projectName + "-validator-jar\"\n";
		execGen = execGen + mavnExecPath + " " + mavenTarget + "\n";
		log.info("executed mvn target : {}", execGen);
		String checkoutScriptPath = file.getAbsolutePath() + File.separator + projectName + "-validator-jar/" + mavenTarget.replace(":", "_") + ".sh";
		FileReadWrite.printDoc(execGen, checkoutScriptPath);
		Runtime.getRuntime().exec("chmod 777 " + checkoutScriptPath);
		Process pr = Runtime.getRuntime().exec("sh " + checkoutScriptPath);
		logExecOutput(pr, mavenTarget);
	}

	private static void extractValueSets(String bbr, String folderOutput) throws IOException {
		String projectName = extractProjectName(bbr);
		Decor decor = DecorMarshaller.loadDecor(bbr);
		String outputResourceFolder = folderOutput + File.separator + projectName + "-validator-jar/target/appassembler/bin/resources";
		File outputFile = new File(outputResourceFolder + "/valueSets/");
		ValueSetExtractor.flattenAndExtractValueSetsFromDecor(decor, outputFile);
	}

	private static void copyAssembledValidationToolToOutput(String bbr, String folderOutput) throws IOException {
		String projectName = extractProjectName(bbr);
		String assemblerPath = folderOutput + File.separator + projectName + "-validator-jar/target/appassembler";
		FileUtils.copyDirectory(new File(assemblerPath), new File(folderOutput + File.separator + projectName + "-validator-app"));
		ZipUtility.zipDirectory(new File(folderOutput + File.separator + projectName + "-validator-app"), 
				new File(folderOutput + File.separator + projectName + "-validator-app.zip"));
	}

	private static void updateCHMODValidateScript(String bbr, String folderOutput) throws IOException {
		String projectName = extractProjectName(bbr);
		String validateScriptPath = folderOutput + File.separator + projectName + "-validator-jar/target/appassembler/bin/validate.sh";
		Runtime.getRuntime().exec("chmod 777 \"" + validateScriptPath + "\"");
	}

	private static void copyResourcesForBinAssembler(String bbr, String folderInput, String folderOutput) throws IOException {
		String projectName = extractProjectName(bbr);
		File fil = new File(folderInput + "/valresources");
		String outputResourceFolder = folderOutput + File.separator + projectName + "-validator-jar/target/appassembler/bin/resources";
		FileUtils.copyDirectory(fil, new File(outputResourceFolder));
	}

	private static void assembleProjectValidator(String bbr, String folderOutput, String mavnExecPath) throws IOException, InterruptedException {
		executeMavenPluginTarget(bbr, folderOutput, mavnExecPath, "package");
	}
	
	private static void updateImportsForValidator(String bbr, String propFile, String folderOutput) throws IOException {
		String projectName = extractProjectName(bbr);
		File file = new File(folderOutput + File.separator + projectName + "-validator-jar/src/main/java/net/ihe/gazelle/" + projectName);
		Properties prop = new Properties();
		prop.load(new FileInputStream(propFile));
		String imports = prop.getProperty("imports");
		if (file.exists()) {
			String[] listImport = imports.split(",");
			StringBuilder repl = new StringBuilder();
			repl.append("import java.util.List;\n");
			for (String imp : listImport) {
				repl.append("import " + imp + ";\n");
			}

			replaceIntoFolder("import java.util.List;", repl.toString(), file.getAbsolutePath());
			replaceIntoFolder("CommonOperationsStaticValidator.", "CommonOperationsStatic.", file.getAbsolutePath());
		}
		
	}

	private static void checkoutNeededModels(String folderOutput) throws IOException, InterruptedException {
		File file = new File(folderOutput);
		String checkout =  CD_COMMAND + file.getAbsolutePath() + "\"\n";
		checkout = checkout + "svn checkout https://scm.gforge.inria.fr/anonscm/svn/gazelle/validators/models/cda-model/trunk cda-model\n";
		checkout = checkout + "svn checkout https://scm.gforge.inria.fr/anonscm/svn/gazelle/validators/models/datatypes-model/trunk datatypes-model\n";
		checkout = checkout + "svn checkout https://scm.gforge.inria.fr/anonscm/svn/gazelle/validators/models/common-models/trunk common-models\n";
		checkout = checkout + "svn checkout https://scm.gforge.inria.fr/anonscm/svn/gazelle/validators/models/nblock-model/trunk nblock-model\n";
		checkout = checkout + "svn checkout https://scm.gforge.inria.fr/anonscm/svn/gazelle/validators/models/voc-model/trunk voc-model\n";
		checkout = checkout + "svn checkout https://scm.gforge.inria.fr/anonscm/svn/gazelle/validators/models/cdaepsos-model/trunk cdaepsos-model\n";
		String checkoutScriptPath = file.getAbsolutePath() + File.separator + "checkout.sh";
		FileReadWrite.printDoc(checkout, checkoutScriptPath);
		Process pr = Runtime.getRuntime().exec(new String[]{"bash","-c", checkout});
		// Log in jboss the InputStream and ErrorStream
		logExecOutput(pr, "checkout");
		
	}
	
	private static void checkoutArchetypes(String folderOutput) throws IOException, InterruptedException {
		File file = new File(folderOutput);
		String checkout =  CD_COMMAND + file.getAbsolutePath() + "\"\n";
		checkout = checkout + "svn checkout https://scm.gforge.inria.fr/anonscm/svn/gazelle/validators/hl7templates/hl7templates-packager-jar/trunk/src/main/resources archetypes\n";

		String checkoutScriptPath = file.getAbsolutePath() + File.separator + "checkout.sh";
		FileReadWrite.printDoc(checkout, checkoutScriptPath);
		Process pr = Runtime.getRuntime().exec(new String[]{"bash","-c", checkout});
		// Log in jboss the InputStream and ErrorStream
		logExecOutput(pr, "checkout");
		
	}
	
	private static void copyProjectModel(String bbr, String folderInput, String folderOutput) throws IOException {
		//This is the prefix without eventual "-"
		String projectName = extractProjectName(bbr);
		File fil = new File(folderInput + "/project-templates/project-model");
		log.info(fil.getAbsolutePath());
		FileUtils.copyDirectory(fil, new File(folderOutput + File.separator + projectName + MODEL_PREFIX));
		FileUtils.copyFileToDirectory(new File(fil.getParentFile().getAbsolutePath() + File.separator + "ocl2java-1.1.4.jar"), new File(folderOutput));
		replaceIntoFolder("${project_name}", projectName + MODEL_PREFIX, folderOutput + File.separator + projectName + MODEL_PREFIX);
		replaceIntoFolder("${folder_output}", folderOutput, folderOutput + File.separator + projectName + MODEL_PREFIX);
		replaceIntoFolder("${project_name_capitalysed_first}", projectName + MODEL_PREFIX, folderOutput + File.separator + projectName + MODEL_PREFIX);
	}

	private static void replaceIntoFolder(String toBeReplaced, String replacement, String folderPath) throws IOException {
		File file = new File(folderPath);
		log.info(file.getAbsolutePath());
		for (File fis : file.listFiles()) {
			if (fis.isDirectory()) {
				replaceIntoFolder(toBeReplaced, replacement, fis.getAbsolutePath());
			}
			else {
				String content = FileUtils.readFileToString(fis);
				content = content.replace(toBeReplaced, replacement);
				FileUtils.writeStringToFile(fis, content, "UTF-8");
			}
			if (fis.getName().contains(toBeReplaced)) {
				boolean renameopp = fis.renameTo(new File(fis.getAbsolutePath().replace(toBeReplaced, replacement)));
				if (!renameopp) {
					log.info("renaming the file has failed : {}", replacement);
				}
			}
		}
	}

	private static String extractProjectName(String bbr) {
		Decor dec = DecorMarshaller.loadDecor(bbr);
		log.info("bbr = {}", bbr);
		log.info("dec = {}", dec);
		log.info("dec.project = {}", dec.getProject());
		log.info("dec.proj.name = {}", dec.getProject().getPrefix());
		String projectName = ProjectFlattenProc.flattenPrefix(dec.getProject().getPrefix());
		if (projectName.endsWith("-")) {
			projectName = projectName.substring(0, projectName.length()-1);
		}
		return projectName;
	}
	
	private static void generateNewModel(String bbr, String folderOutput, Boolean ignoreTemplateIdRequirements, String versionLabel) throws JAXBException, IOException {
		HL7TemplatesConverter templatesConverter = new HL7TemplatesConverter();
		GenerationProperties.ignoreAssertionGeneration = false;
		// umlModelForBBR will contain the uml model for the bbr
		String umlModelForBBR = templatesConverter.convertArtDecorXMI(bbr, ignoreTemplateIdRequirements, versionLabel);
		log.info("BBR converted to UML.");
		String projectName = extractProjectName(bbr);
		File outUmlModelPath = new File(folderOutput + File.separator + projectName + MODEL_PREFIX + File.separator + "model" + File.separator + projectName + ".uml");
		FileUtils.writeStringToFile(outUmlModelPath, umlModelForBBR, "UTF-8");
		HL7TemplatesConverter.logProblems(folderOutput);
	}
	
	private static void copyProjectValidator(String bbr, String folderInput, String folderOutput, String serviceNameParam, String validatorRootClass) throws IOException {
		String loadMethod;
		String projectName = extractProjectName(bbr);
		String serviceName = serviceNameParam!=null?serviceNameParam:projectName;
		File fil = new File(folderInput + "/project-templates/project-validator-jar");
		String outputJArFolder = folderOutput + File.separator + projectName + "-validator-jar";
		String cap = projectName.substring(0, 1).toUpperCase() + projectName.substring(1);
		FileUtils.copyDirectory(fil, new File(outputJArFolder));
		replaceIntoFolder("${project_name}", projectName, outputJArFolder);
		replaceIntoFolder("${project_name_capitalysed}", projectName.toUpperCase(), outputJArFolder);
		replaceIntoFolder("${project_name_capitalysed_first}", cap, outputJArFolder);
		replaceIntoFolder("${folder_output}", folderOutput, outputJArFolder);
		replaceIntoFolder("${project.service.name}", serviceName, outputJArFolder);
		if (validatorRootClass == null) {
			validatorRootClass = "net.ihe.gazelle.cda.POCDMT000040ClinicalDocument";
		}
		replaceIntoFolder("${validator_root_class}", validatorRootClass, outputJArFolder);
		if (validatorRootClass.equals("net.ihe.gazelle.cda.POCDMT000040ClinicalDocument")){
			loadMethod = "loadBasic";
		} else {
			loadMethod="load";
		}
		replaceIntoFolder("${load_method}", loadMethod, outputJArFolder);
	}
	
	private static void generateProjectValidator(String bbr, String folderOutput, String mavnExecPath) throws IOException, InterruptedException {
		executeMavenPluginTarget(bbr, folderOutput, mavnExecPath, "mbgen:genvalidator");
	}
	
	private static void generateProjectCDATemplatesTree(String bbr, String folderOutput, String mavnExecPath) throws IOException, InterruptedException {
		executeMavenPluginTarget(bbr, folderOutput, mavnExecPath, "mbgen:gencdatree");
	}

	private static void logExecOutput(Process pr, String processName) throws IOException,
			InterruptedException {
		log.info("Process : {}", processName);
		BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
	    String line;
	    while ((line = in.readLine()) != null) {
	    	log.info(line);
	    }
	    in = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
	    while ((line = in.readLine()) != null) {
	    	log.info(line);
	    }
	    pr.waitFor();
	    log.info("ok!");

	    in.close();
	}

	private static void treateOutputFolder(String folderOutput) throws IOException {
		File folderOut = new File(folderOutput);
		if (folderOut.isFile()) {
			throw new FileNotFoundException("The output Folder is a file !");
		}
		if (folderOut.exists()) {
			FileUtils.forceDelete(folderOut);
		}
		folderOut.mkdirs();
	}
	
	private static void logSeparator(String message){
		log.info("\n");
		log.info("//////////////////////////////////////////////////////");
		log.info(message);
		log.info("//////////////////////////////////////////////////////");
		log.info("\n");
	}
}
