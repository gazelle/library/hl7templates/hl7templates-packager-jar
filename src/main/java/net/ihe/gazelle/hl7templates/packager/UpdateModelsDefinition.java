package net.ihe.gazelle.hl7templates.packager;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempgen.action.FileReadWrite;

public final class UpdateModelsDefinition {
	
	private UpdateModelsDefinition() {
		// private constructor
	}
	
	
	private static Logger log = LoggerFactory.getLogger(UpdateModelsDefinition.class);
	
	public static void updateModelDefinitions(Boolean notOverride, String folderOutput) {
		if (!notOverride) {
			String pathResources = System.getProperty("MODELS_DESCRIBER",System.getProperty("HL7TEMP_RESOURCES_PATH", "../hl7templates-resources") + 
					File.separator + 
					System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") + 
					"/modelsDefinition.xml");
			try {
				String content = FileReadWrite.readDoc(pathResources);
				String folderOutputformatted = folderOutput.endsWith("/")?folderOutput.substring(0, folderOutput.length()-1):folderOutput;
				String replaceBy = updateWorkspacePath(content, folderOutputformatted);
				FileReadWrite.printDoc(replaceBy, pathResources);
			} catch (IOException e) {
				log.info("modelsDefinition.xml not found", e);
			}
			
		}
	}

	public static String updateWorkspacePath(String content, String folderOutputformatted) {
		if (content != null) {
			String regex = "<path>(.*?)</path>";
			Pattern pat = Pattern.compile(regex);
			Matcher mat = pat.matcher(content);
			String tobeReplaced = "";
			if(mat.find()) {
				String stringUMLPath = mat.group(1);
				tobeReplaced = extractWorkspace(stringUMLPath);
			}
			return content.replace(tobeReplaced, folderOutputformatted);
		}
		return null;
	}

	public static String extractWorkspace(String stringUMLPath) {
		String[] ss = stringUMLPath.split("/");
		for (String string : ss) {
			if (string.contains("-model")) {
				return stringUMLPath.substring(0, stringUMLPath.indexOf(string)-1);
			}
		}
		return "";
	}

}
