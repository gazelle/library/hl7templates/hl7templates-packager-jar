package net.ihe.gazelle.hl7templates.packager;

import org.apache.log4j.BasicConfigurator;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HL7Templates2GOC {

    private static Logger log = LoggerFactory.getLogger(HL7Templates2GOC.class);

    static final String DOCUMENTATION_STRING = "\nHL7Templates2GOC -out outputFolder -bbr buildBlockRepositoryURL";

    @Option(name = "-out", metaVar = "outputFile", usage = "Output folder", required = true)
    private String outputFile;

    @Option(name = "-in", metaVar = "inputFile", usage = "Input folder", required = false)
    private String inputFile;

    @Option(name = "-bbr", metaVar = "bbr", usage = "Output folder", required = true)
    private String bbr;

    @Option(name = "-serviceName", metaVar = "serviceName", usage = "Validation Service Name", required = false)
    private String serviceName;

    @Option(name = "-prop", metaVar = "properties", usage = "Path to customized java imports file", required = false)
    private String properties;

    @Option(name = "-mvn", metaVar = "mvn", usage = "maven executable path", required = false)
    private String mvnExecPath = "mvn";

    @Option(name = "-notoverride", usage = "Not override the modelDefinition.xml file", required = false)
    private Boolean notOverride = false;

    @Option(name = "-ignoreTemplateIdRequirements", usage = "ignore TemplateId Requirements when generating the contraints (useful for C-CDA " +
            "validation tools)", required = false)
    private Boolean ignoreTemplateIdRequirements = false;

    @Option(name = "-versionLabel", metaVar = "versionLabel", usage = "versionLabel that will be used during the generation", required = false)
    private String versionLabel;

    @Option(name = "-rootClassName", metaVar = "rootClassName", usage = "Root Class Name", required = false)
    private String rootClassName;

    @Option(name = "-HL7TEMP_CDACONFFOLDERNAME", metaVar = "hl7temp_cdaconffoldername", usage = "hl7temp cda conf folder name", required = false)
    private String HL7TEMP_CDACONFFOLDERNAME;


    /**
     * @return the rootClassName
     */
    public String getRootClassName() {
        return rootClassName;
    }

    /**
     * @param rootClassName the rootClassName to set
     */
    public void setRootClassName(String rootClassName) {
        this.rootClassName = rootClassName;
    }

    public String getVersionLabel() {
        return versionLabel;
    }

    public void setVersionLabel(String versionLabel) {
        this.versionLabel = versionLabel;
    }

    public Boolean getIgnoreTemplateIdRequirements() {
        return ignoreTemplateIdRequirements;
    }

    public void setIgnoreTemplateIdRequirements(Boolean ignoreTemplateIdRequirements) {
        this.ignoreTemplateIdRequirements = ignoreTemplateIdRequirements;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMvnExecPath() {
        return mvnExecPath;
    }

    public void setMvnExecPath(String mvnExecPath) {
        this.mvnExecPath = mvnExecPath;
    }

    public String getInputFile() {
        if (inputFile == null) {
            inputFile = getOutputFile() + "/archetypes/";
        }
        return inputFile;
    }

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public String getProperties() {
        if (properties == null) {
            properties = getOutputFile() + "/archetypes/conf.properties";
        }
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public Boolean getNotOverride() {
        return notOverride;
    }

    public void setNotOverride(Boolean notOverride) {
        this.notOverride = notOverride;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    public String getBbr() {
        return bbr;
    }

    public void setBbr(String bbr) {
        this.bbr = bbr;
    }

    public String getHL7TEMP_CDACONFFOLDERNAME() {
        return HL7TEMP_CDACONFFOLDERNAME;
    }

    public void setHL7TEMP_CDACONFFOLDERNAME(String HL7TEMP_CDACONFFOLDERNAME) {
        this.HL7TEMP_CDACONFFOLDERNAME = HL7TEMP_CDACONFFOLDERNAME;
    }

    public void execute(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        try {
            parser.parseArgument(args);
            if (this.bbr == null || this.outputFile == null) {
                log.info(HL7Templates2GOC.DOCUMENTATION_STRING);
                return;
            }
            if (getHL7TEMP_CDACONFFOLDERNAME() != null && !getHL7TEMP_CDACONFFOLDERNAME().isEmpty()) {
                log.info("HL7TEMP_CDACONFFOLDERNAME set to " + getHL7TEMP_CDACONFFOLDERNAME());
                System.setProperty("HL7TEMP_CDACONFFOLDERNAME", getHL7TEMP_CDACONFFOLDERNAME());
            }
            GOCPackager.packageBBRToGOCValidationTool(getBbr(), getServiceName(), getOutputFile(), getInputFile(),
                    getProperties(), getNotOverride(), getMvnExecPath(), getIgnoreTemplateIdRequirements(), getVersionLabel(), getRootClassName());
        } catch (Exception e) {
            log.error("Exception in the execution of cmmand line", e);
        }
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();
        HL7Templates2GOC gaz = new HL7Templates2GOC();
        gaz.execute(args);
    }

}
