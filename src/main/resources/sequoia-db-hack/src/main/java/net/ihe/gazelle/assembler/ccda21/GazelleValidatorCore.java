package net.ihe.gazelle.assembler.ccda21;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.db.utils.ConnectionUtil;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.validation.DetailedResult;

public class GazelleValidatorCore {
	
	static {
		ConnectionUtil cu = new ConnectionUtil();
		CommonOperations.setValuesetChecker(cu);
	}
	
	private GazelleValidatorCore(){}
	
	public static String validateDocument(String documentPath) throws Exception{
		ConnectionUtil.init();
		String res = "";
		try {
			DetailedResult dr = MicroDocumentValidation.validateUsingGeneratedGOC(documentPath);
			res = getDetailedResultAsString(dr);
			if (res.contains("?>")){
				res = res.substring(res.indexOf("?>") + 2);
			}
		}
		finally {
			ConnectionUtil.close();
		}
		return res;
	}
	
	private static String getDetailedResultAsString(DetailedResult dr){
		if (dr != null){
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				DetailedResultTransformer.save(baos, dr);
			} catch (JAXBException e) {
				e.printStackTrace();
			}
			String res = baos.toString();
			res = deleteUnicodeZero(res);
			return res;
		}
		return null;
	}
	
	private static String deleteUnicodeZero(String s) {
		return s.replaceAll("[\\u0000]", "");
	}
	
}
