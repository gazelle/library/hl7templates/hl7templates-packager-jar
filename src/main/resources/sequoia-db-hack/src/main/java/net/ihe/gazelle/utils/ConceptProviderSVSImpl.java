package net.ihe.gazelle.utils;

import java.util.List;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.ConceptProviderImpl;

public class ConceptProviderSVSImpl extends ConceptProviderImpl {
	
	@Override
	public List<Concept> getConceptFromValueSet(String valueSetId, String conceptCode, String conceptCodeSystem,
			String lang, String version) {
		System.out.println(getSVSRepositoryUrl() + "?id=" + valueSetId + "&code=" + conceptCode);
		return super.getConceptFromValueSet(valueSetId, conceptCode, conceptCodeSystem, lang, version);
	}
	
	@Override
	protected String getSVSRepositoryUrl() {
		return "https://gazellecontent.sequoiaproject.org/SVSSimulator/rest/RetrieveValueSetForSimulator";
	}

}
