package net.ihe.gazelle.db.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.ihe.gazelle.gen.common.ConceptProvider;
import net.ihe.gazelle.gen.common.ValueSetProvider;
import net.ihe.gazelle.gen.common.ValuesetChecker;

public class ConnectionUtil implements ValuesetChecker {

	private static Connection c = null;

	private static PreparedStatement stmtMatchesCodeToValueSet = null;

	private static PreparedStatement stmtMatchesValueSet = null;
	
	private static Map<String, List<String>> cacheQueries = new HashMap<>();

	public static void init() {
		if (c == null) {
			try {
				String url = "jdbc:postgresql://localhost:5432/svs-simulator";
				Properties props = new Properties();
				props.setProperty("user","gazelle");
				props.setProperty("password","gazelle");
				Class.forName("org.postgresql.Driver");
				c = DriverManager.getConnection(url, props);
				initPreStat1();
				initPreStat2();
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(e.getClass().getName()+": "+e.getMessage());
				System.exit(0);
			}
			System.out.println("Opened database successfully");
		}
	}

	private static void initPreStat1() {
		String sql = "select count(svs_concept.code) as cc from svs_concept where code=? and "
				+ "concept_list_id in (select id from svs_conceptlist where value_set_id in "
				+ "(select id from svs_value_set where vs_id_oid =?));";
		try {
			ConnectionUtil.stmtMatchesCodeToValueSet = c.prepareStatement(sql);
			((org.postgresql.PGStatement)stmtMatchesCodeToValueSet).setPrepareThreshold(1);
		} catch (SQLException e) {
			e.printStackTrace();
			if (ConnectionUtil.stmtMatchesCodeToValueSet != null) {
				try {
					ConnectionUtil.stmtMatchesCodeToValueSet.close();
					ConnectionUtil.stmtMatchesCodeToValueSet = null;
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	private static void initPreStat2() {
		String sql = "select count(svs_concept.code) as cc from svs_concept where code=? and codesystem=? and "
				+ "concept_list_id in (select id from svs_conceptlist where value_set_id in "
				+ "(select id from svs_value_set where vs_id_oid =?));";
		try {
			ConnectionUtil.stmtMatchesValueSet = c.prepareStatement(sql);
			((org.postgresql.PGStatement)stmtMatchesValueSet).setPrepareThreshold(1);
		} catch (SQLException e) {
			e.printStackTrace();
			if (ConnectionUtil.stmtMatchesValueSet != null) {
				try {
					ConnectionUtil.stmtMatchesValueSet.close();
					ConnectionUtil.stmtMatchesValueSet = null;
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public static void close() {
		if (c != null) {
			try {
				c.close();
				stmtMatchesCodeToValueSet.close();
				stmtMatchesValueSet.close();
				System.out.println("Opened database successfully closed");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setValueSetProvider(ValueSetProvider valueSetProvider) {
		return;
	}

	@Override
	public void setConceptProvider(ConceptProvider conceptProvider) {
		return;
	}

	@Override
	public Boolean matchesValueSet(String oidparam, String code, String codeSystem, String codeSystemName,
			String displayName) {
		if (oidparam == null || code == null || codeSystem == null) {
			return false;
		}
		oidparam = oidparam.split("&")[0];
		if (cacheQueries.containsKey(oidparam) && cacheQueries.get(oidparam).contains(code + ":" + codeSystem)) {
			return true;
		}
		if (c != null && stmtMatchesValueSet != null) {
			try {
				stmtMatchesValueSet.setString(1, code);
				stmtMatchesValueSet.setString(2, codeSystem);
				stmtMatchesValueSet.setString(3, oidparam);
				ResultSet res = stmtMatchesValueSet.executeQuery();
				int count = 0;
				if (res.next()) {
					count = res.getInt("cc");
				}
				if (count > 0){
					if (!cacheQueries.containsKey(oidparam)) {
						cacheQueries.put(oidparam, new ArrayList<String>());
					}
					cacheQueries.get(oidparam).add(code + ":" + codeSystem);
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public Boolean matchesCodeToValueSet(String oidparam, String code) {
		if (oidparam == null || code == null ) {
			return false;
		}
		oidparam = oidparam.split("&")[0];
		if (cacheQueries.containsKey(oidparam) && cacheQueries.get(oidparam).contains(code)) {
			return true;
		}
		if (c != null && stmtMatchesCodeToValueSet != null) {
			try {
				stmtMatchesCodeToValueSet.setString(1, code);
				stmtMatchesCodeToValueSet.setString(2, oidparam);
				ResultSet res = stmtMatchesCodeToValueSet.executeQuery();
				int count = 0;
				if (res.next()) {
					count = res.getInt("cc");
				}
				if (count > 0){
					if (!cacheQueries.containsKey(oidparam)) {
						cacheQueries.put(oidparam, new ArrayList<String>());
					}
					cacheQueries.get(oidparam).add(code);
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public Boolean matchesValueSetWithDisplayName(String oidparam, String code, String codeSystem,
			String codeSystemName, String displayName) {
		return matchesValueSet(oidparam, code, codeSystem, codeSystemName, displayName);
	}

	public static void main(String[] args) {
		ConnectionUtil cu = new ConnectionUtil();
		init();
		boolean ss = cu.matchesCodeToValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "16932000");
		System.out.println(ss);
		close();
	}

}
