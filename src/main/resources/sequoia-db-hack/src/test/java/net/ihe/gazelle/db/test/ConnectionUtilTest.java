package net.ihe.gazelle.db.test;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import net.ihe.gazelle.db.utils.ConnectionUtil;

public class ConnectionUtilTest {
	
	@BeforeClass
	public static void beforeClass() {
		ConnectionUtil.init();
	}
	
	@AfterClass
	public static void afterClass() {
		ConnectionUtil.close();
	}

	@Test
	public void testMatchesValueSet() {
		ConnectionUtil cu = new ConnectionUtil();
		long startTime = System.currentTimeMillis();
		boolean ss = cu.matchesValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "16932000", "2.16.840.1.113883.6.96", "", "");
		assertTrue(ss);
		ss = cu.matchesValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "dddd", "2.16.840.1.113883.6.96", "", "");
		assertFalse(ss);
		ss = cu.matchesValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "16932000", "2.16.840.1.113883.6.96", "", "");
		assertTrue(ss);
		ss = cu.matchesValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "16932000", "sssss", "", "");
		assertFalse(ss);
		for(int i=0;i<2000;i++) {
			ss = cu.matchesValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "16932000", "2.16.840.1.113883.6.96", "", "");
		}
		long endTime   = System.currentTimeMillis();
		long totalTime = (endTime - startTime);
		System.out.println("the execution time is:");
	    System.out.println(totalTime);
		assertTrue(ss);
	}

	@Test
	public void testMatchesCodeToValueSet() {
		ConnectionUtil cu = new ConnectionUtil();
		boolean ss = cu.matchesCodeToValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "16932000");
		assertTrue(ss);
		ss = cu.matchesCodeToValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "xxx");
		assertFalse(ss);
		ss = cu.matchesCodeToValueSet("2.16.840.1.113883.3.88.12.3221.7.4", "16932000");
		assertTrue(ss);
	}

	@Test
	public void testMatchesValueSetWithDisplayName() {

	}

}
