package net.ihe.gazelle.assembler.${project_name};

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.Marshaller.Listener;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Note;

class DetailedResultTransformer {
	
	public static DetailedResult load(InputStream is) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance(DetailedResult.class);
		Unmarshaller u = jc.createUnmarshaller();
		DetailedResult res = (DetailedResult) u.unmarshal(is);
		return res;
	}
	
	public static void save(OutputStream os, DetailedResult txdw) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance(DetailedResult.class);
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF8");
		m.setListener(new Listener() {
			
			@Override
			public void beforeMarshal(Object source) {
				if (source instanceof MDAValidation) {
					MDAValidation mda = (MDAValidation)source;
					List<Note> ln = new ArrayList<>();
					for (Object obj : mda.getWarningOrErrorOrNote()) {
						if (obj instanceof Note && ((Note)obj).getTest().matches("(cdadt.*)|(rmim.*)|(dtits.*)")) {
							ln.add((Note)obj);
						}
					}
					mda.getWarningOrErrorOrNote().removeAll(ln);
				}
			}
		});
		m.marshal(txdw, os);
	}
	
}
