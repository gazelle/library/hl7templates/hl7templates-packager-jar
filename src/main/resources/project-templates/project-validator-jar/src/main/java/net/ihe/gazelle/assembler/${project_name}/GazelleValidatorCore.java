package net.ihe.gazelle.assembler.${project_name};

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.utils.ValueSetProviderClass;

public class GazelleValidatorCore {
	
	static {
		CommonOperations.setValueSetProvider(new ValueSetProviderClass());
	}
	
	private GazelleValidatorCore(){}
	
	public static String validateDocument(String documentPath) throws Exception{
		DetailedResult dr = MicroDocumentValidation.validateUsingGeneratedGOC(documentPath);
		String res = getDetailedResultAsString(dr);
		if (res.contains("?>")){
			res = res.substring(res.indexOf("?>") + 2);
		}
		return res;
	}
	
	private static String getDetailedResultAsString(DetailedResult dr){
		if (dr != null){
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				DetailedResultTransformer.save(baos, dr);
			} catch (JAXBException e) {
				e.printStackTrace();
			}
			String res = baos.toString();
			res = deleteUnicodeZero(res);
			return res;
		}
		return null;
	}
	
	private static String deleteUnicodeZero(String s) {
		return s.replaceAll("[\\u0000]", "");
	}
	
}
