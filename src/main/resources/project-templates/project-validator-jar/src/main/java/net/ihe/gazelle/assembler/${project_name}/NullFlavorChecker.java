package net.ihe.gazelle.assembler.${project_name};

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class NullFlavorChecker {

    public String flatten(Object object, String location, List<Notification> notificationList) {

        Node node;
        String newLocation;
        if (object instanceof Node && ((Node) object).getNodeType() == Node.ELEMENT_NODE){
            node = (Node) object;
            boolean nullFlavored = false;
            int otherAttributeSize = 0;
            NamedNodeMap attributeList = node.getAttributes();
            NodeList children = node.getChildNodes();

            for (int i = 0; i < attributeList.getLength() ; i++){
                Node attribute = attributeList.item(i);
                if (attribute.getNodeName().equals("nullFlavor")){
                    nullFlavored = true;
                } else if (!attribute.getNodeName().contains("xmlns")) {
                    otherAttributeSize +=1;
                }
            }
            if (nullFlavored){
                if (children.getLength() > 0 || otherAttributeSize > 0 || node.getNodeValue() != null) {
                    Notification notif = new Warning();
                    notif.setTest("NullFlavorChecker");
                    notif.setDescription("In "+ location + " nullFlavor is defined but the element still defines attributes and sub-elements");
                    notif.setLocation(location);
                    notif.setIdentifiant("NullFlavorChecker");
                    notif.setType("Null Flavor Check");
                    notificationList.add(notif);
                    if (false){
                        removeChildrenAndAttributes(node);
                    }
                }
            } else {
                for(int i= 0 ; i < children.getLength(); i++){
                    newLocation = location + "/" + children.item(i).getNodeName();
                    if (isArrayNode(node)){
                        newLocation += "[" + getIndexOfArrayNode(node) + "]";
                    }
                    flatten(children.item(i), newLocation, notificationList);
                }
            }
            return nodeToString(node);
        }
        return null;
    }

    private void removeChildrenAndAttributes(Node node){
        Node child = node.getFirstChild();
        NamedNodeMap attributeList = node.getAttributes();
        List<Node> childrenToRemove = new ArrayList<>();

        while(child!=null){
            childrenToRemove.add(child);
            child = child.getNextSibling();
        }
        for (Node childToRemove:  childrenToRemove){
            node.removeChild(childToRemove);
        }
        for (int i = 0 ; i < attributeList.getLength() ; i++){
            Node attribute = node.getAttributes().item(i);
            if (!attribute.getNodeName().equals("nullFlavor") && !attribute.getNodeName().contains("xmlns") && !attribute.getNodeName().contains("xsi:type")){
                node.getAttributes().removeNamedItem(attribute.getNodeName());
        }
        }
        node.setNodeValue(null);
    }

    private static String getNodeDisplayName(Node node) {
        if (node.getPrefix()!=null && !node.getNodeName().contains(":")){
            return node.getPrefix() + ":" + node.getNodeName();
        }
        return node.getNodeName();
    }

    private static boolean isArrayNode(Node node) {
        if (node.getNextSibling() == null && node.getPreviousSibling() == null) {
            return false;
        } else {
            Node currentNode = node.getPreviousSibling();
            while(currentNode!=null){
                if (getNodeDisplayName(currentNode).equals(getNodeDisplayName(node))){
                    return true;
                }
                currentNode = currentNode.getPreviousSibling();
            }
            currentNode = node.getNextSibling();
            while(currentNode!=null){
                if (getNodeDisplayName(currentNode).equals(getNodeDisplayName(node))){
                    return true;
                }
                currentNode = currentNode.getNextSibling();
            }
            return false;
        }
    }

    private static Integer getIndexOfArrayNode(Node node) {
        if(isArrayNode(node)) {
            int leftCount = 0;
            Node currentNode = node.getPreviousSibling();
            while(currentNode != null) {
                if (getNodeDisplayName(currentNode).equals(getNodeDisplayName(node))){
                    leftCount++;
                }
                currentNode = currentNode.getPreviousSibling();
            }
            return leftCount;
        } else {
            return null;
        }
    }

    public static String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.setOutputProperty(OutputKeys.INDENT, "no");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            System.out.println("nodeToString Transformer Exception");
        }
        return sw.toString();
    }
}
