package net.ihe.gazelle.assembler.${project_name};

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import net.ihe.gazelle.utils.ProjectDependencies;

import org.apache.commons.codec.binary.Base64;
import org.apache.xalan.processor.TransformerFactoryImpl;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.ExampleMode;
import org.kohsuke.args4j.Option;

/**
 * 
 * @author abderrazek boufahja
 * 
 */
public class Gazelle${project_name_capitalysed}Validator {

	final static String documentationString = "\nGazelle${project_name_capitalysed}Validator (-path documentPath |-b64 documentB64)  [-out outputFile] [-html] [-resources pathToResourcesFolder]";

	@Option(name = "-html", usage = "generate html as output of validation", required = false)
	private Boolean generateHTML = false;

	@Option(name = "-help", usage = "how to use the tool", required = false)
	private Boolean viewHelp = false;

	@Option(name = "-path", metaVar = "documentPath", usage = "document path", required = false)
	private String documentPath;

	@Option(name = "-b64", metaVar = "documentB64", usage = "document on base 64", required = false)
	private String documentB64;

	@Option(name = "-out", metaVar = "outputFile", usage = "path to the output file", required = false)
	private String outputFilePath;

	@Option(name = "-resources", metaVar = "resourcesFolderPath", usage = "full path to the resources folder (XSD schema + valueSets), "
			+ "example : 'c:/users/.../resources/' ", required = false)
	private String resourcesFolderPath;

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

	public String getResourcesFolderPath() {
		return resourcesFolderPath;
	}

	public void setResourcesFolderPath(String resourcesFolderPath) {
		this.resourcesFolderPath = resourcesFolderPath;
	}

	public Boolean getViewHelp() {
		return viewHelp;
	}

	public void setViewHelp(Boolean viewHelp) {
		this.viewHelp = viewHelp;
	}

	public Boolean getGenerateHTML() {
		return generateHTML;
	}

	public void setGenerateHTML(Boolean generateHTML) {
		this.generateHTML = generateHTML;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getDocumentB64() {
		return documentB64;
	}

	public void setDocumentB64(String documentB64) {
		this.documentB64 = documentB64;
	}

	public static void main(String[] args) {
		Gazelle${project_name_capitalysed}Validator gaz = new Gazelle${project_name_capitalysed}Validator();
		gaz.execute(args);
	}

	public void execute(String[] args) {
		CmdLineParser parser = new CmdLineParser(this);
		try {
			parser.parseArgument(args);

			if (this.viewHelp) {
				System.out.println(Gazelle${project_name_capitalysed}Validator.documentationString);
				parser.printUsage(System.out);
				parser.printExample(ExampleMode.ALL);
				return;
			}

			if (documentPath == null && documentB64 == null) {
				throw new Exception(
						"One and only one of these two arguments shall be present : -path / -b64");
			}

			if (documentPath != null && documentB64 != null) {
				throw new Exception(
						"One and only one of these two arguments shall be present : -path / -b64");
			}

			if (resourcesFolderPath != null && !resourcesFolderPath.equals("")) {
				ProjectDependencies.CDA_XSD = resourcesFolderPath
						+ File.separator + "xsd" + File.separator + "CDA.xsd";
				ProjectDependencies.CDA_XSL_TRANSFORMER = resourcesFolderPath
						+ File.separator + "mbvalidatorDetailedResult.xsl";
				ProjectDependencies.VALUE_SET_REPOSITORY = resourcesFolderPath
						+ File.separator + "valueSets" + File.separator;
			}

			String valRes = null;
			if (this.documentPath != null) {
				valRes = GazelleValidatorCore.validateDocument(documentPath);
			} else {
				byte[] buffer = Base64.decodeBase64(documentB64.getBytes());
				if (buffer != null) {
					String document = new String(buffer);
					valRes = GazelleValidatorCore.validateDocument(document);
				} else {

				}
			}

			if (this.generateHTML) {
				try {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("viewdown", false);
					map.put("constraintPath", false);
					valRes = resultTransformation(new ByteArrayInputStream(
							valRes.getBytes()), new FileInputStream(
							ProjectDependencies.CDA_XSL_TRANSFORMER), map);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (outputFilePath != null) {
				FileReadWrite.printDoc(valRes, outputFilePath);
				System.out.println("The output file was updated ("
						+ outputFilePath + ")");
			} else {
				System.out.println(valRes);
			}
		} catch (Exception e) {
			if (this.viewHelp) {
				System.out.println(Gazelle${project_name_capitalysed}Validator.documentationString);
				parser.printUsage(System.out);
				System.out
						.println("sample : -html -out outputFile -path documentPath -val validatorName");
				return;
			}

			System.err.println(e.getMessage());
			System.out.println(Gazelle${project_name_capitalysed}Validator.documentationString);
			parser.printUsage(System.err);
		}
	}

	public static String resultTransformation(InputStream docInputStream,
			InputStream inXslAbsolutePath, Map<String, Object> mapParam) {
		if (docInputStream == null)
			return null;

		if (inXslAbsolutePath == null)
			return null;

		try {
			TransformerFactory tFactory = new TransformerFactoryImpl();

			Transformer transformer = tFactory
					.newTransformer(new javax.xml.transform.stream.StreamSource(
							inXslAbsolutePath));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setParameter(OutputKeys.ENCODING, "UTF8");
			if (mapParam != null) {
				List<String> listOfParameters = new ArrayList<String>(
						mapParam.keySet());
				for (String param : listOfParameters) {
					transformer.setParameter(param, mapParam.get(param));
				}
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			StreamResult out = new StreamResult(baos);
			transformer.transform(new javax.xml.transform.stream.StreamSource(
					docInputStream), out);
			String tmp = new String(baos.toByteArray());
			return tmp;
		} catch (Exception e) {
			e.printStackTrace();
			return "The document cannot be displayed using this stylesheet";
		}
	}

}
