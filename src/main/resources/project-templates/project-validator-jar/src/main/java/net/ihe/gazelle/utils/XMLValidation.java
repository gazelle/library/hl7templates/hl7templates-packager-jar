package net.ihe.gazelle.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;

public class XMLValidation {

	private static SAXParserFactory factoryBASIC;
	
	private static SAXParserFactory factoryCDA;

	static{
		try {
			factoryBASIC = SAXParserFactory.newInstance();
			factoryBASIC.setValidating(false);
			factoryBASIC.setNamespaceAware(true);
		} catch (Exception e){}
		
		try {
			factoryCDA = new com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl();
			factoryCDA.setNamespaceAware( true);
			SchemaFactory sfactory = 
					SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			Schema schema = sfactory.newSchema(new File(ProjectDependencies.CDA_XSD));
			factoryCDA.setSchema(schema);
		} catch (Exception e){
			e.printStackTrace();
		}

	}

	/**
	 * Parses the file using SAX to check that it is a well-formed XML file
	 * @param file
	 * @return
	 */
	public static DocumentWellFormed isXMLWellFormed(String documentPath){
		return validateIfDocumentWellFormedXML(documentPath, "", factoryBASIC);
	}
	
	private static DocumentWellFormed validateIfDocumentWellFormedXML(String documentPath, String xsdpath, SAXParserFactory factory){
		DocumentWellFormed dv = new DocumentWellFormed();
		return validXMLUsingXSD(documentPath, xsdpath, factory, dv);
	}


	/**
	 * 
	 * @param file
	 * @param xsdLocation
	 * @return
	 */
	private static DocumentValidXSD validXMLUsingXSD(String documentPath)
	{
		if (factoryCDA == null || factoryCDA.getSchema() == null){
			DocumentValidXSD res = new DocumentValidXSD();
			res.setResult("FAILED");
			res.setNbOfErrors("1");
			res.getXSDMessage().add(new XSDMessage());
			res.getXSDMessage().get(0).setColumnNumber(0);
			res.getXSDMessage().get(0).setLineNumber(0);
			res.getXSDMessage().get(0).setSeverity("error");
			res.getXSDMessage().get(0).setMessage("The XSD schema is not well configured on the application ! Please contact the administrator to fix this.");
			return  res;
		}
		return validXMLUsingXSD(documentPath, ProjectDependencies.CDA_XSD, factoryCDA);
	}
	
	private static DocumentValidXSD validXMLUsingXSD(String documentPath, String xsdpath, SAXParserFactory factory)
	{
		DocumentValidXSD dv = new DocumentValidXSD();
		return validXMLUsingXSD(documentPath, xsdpath, factory, dv);
	}
	
	private static <T extends DocumentValidXSD> T validXMLUsingXSD(String documentPath, String xsdpath, SAXParserFactory factory, T dv)
	{
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			FileInputStream fis = new FileInputStream(documentPath);
			exceptions =XSDValidator.validateUsingFactoryAndSchema(fis, xsdpath,  factory);
		} catch(Exception e){
			exceptions.add(handleException(e));
		}
		return extractValidationResult(exceptions, xsdpath, factory, dv);
	}
	
	private static <T extends DocumentValidXSD> T extractValidationResult(List<ValidationException> exceptions, String xsdpath, SAXParserFactory factory, T dv)
	{

		dv.setResult("PASSED");

		if (exceptions == null || exceptions.size() == 0)
		{	
			dv.setResult("PASSED");
			return dv;
		}
		else
		{
			Integer nbOfErrors = 0;
			Integer nbOfWarnings = 0;
			Integer exceptionCounter = 0;
			for (ValidationException ve : exceptions)
			{
				if (ve.getSeverity() == null){
					ve.setSeverity("error");
				}
				exceptionCounter ++;
				if ((ve.getSeverity() != null) && (ve.getSeverity().equals("warning"))){
					nbOfWarnings ++;
				}
				else {
					nbOfErrors ++;
				}
				XSDMessage xsd = new XSDMessage();
				xsd.setMessage(ve.getMessage());
				xsd.setSeverity(ve.getSeverity());
				if (isNumeric(ve.getLineNumber())){
					xsd.setLineNumber(Integer.valueOf(ve.getLineNumber()));
				}
				if (isNumeric(ve.getColumnNumber())){
					xsd.setColumnNumber(Integer.valueOf(ve.getColumnNumber()));
				}
				dv.getXSDMessage().add(xsd);
			}
			dv.setNbOfErrors(nbOfErrors.toString());
			dv.setNbOfWarnings(nbOfWarnings.toString());
			if (nbOfErrors > 0){
				dv.setResult("FAILED");
			}
			return dv;
		}

	}
	
	private static boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (Character.isDigit(str.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

	private static ValidationException handleException(Exception e)
	{
		ValidationException ve = new ValidationException();
		ve.setLineNumber("0");
		ve.setColumnNumber("0");
		if (e != null && e.getMessage() != null){
			ve.setMessage("error on validating : " + e.getMessage());
		} 
		else if (e != null && e.getCause() != null && e.getCause().getMessage() != null){
			ve.setMessage("error on validating : " + e.getCause().getMessage());
		}
		else{
			ve.setMessage("error on validating. The exception generated is of kind : " + e.getClass().getSimpleName());
		}
		ve.setSeverity("error");
		return ve;
	}


	/**
	 * Checks that the CDA document is valid (uses CDA.xsd file)
	 * @param file
	 * @return
	 */
	public static DocumentValidXSD isXSDValid(String documentPath)
	{
		if (documentPath == null){
			return null;
		}
		else {
			return validXMLUsingXSD(documentPath);
		}
	}
	
}
