package net.ihe.gazelle.pack.test;

import static org.junit.Assert.*;
import net.ihe.gazelle.hl7templates.packager.UpdateModelsDefinition;

import org.junit.Test;

public class UpdateModelsDefinitionTest {

	@Test
	public void testUpdateWorkspacePath() {
		String ex = UpdateModelsDefinition.updateWorkspacePath("<modelUMLDesc>"
				+ "<path>/path/on/my/machine/voc-model/models/voc.uml</path>"
				+ "aa", "ee");
		assertTrue(ex.equals("<modelUMLDesc>"
				+ "<path>ee/voc-model/models/voc.uml</path>"
				+ "aa"));
	}

	@Test
	public void testExtractWorkspace() {
		String ex = UpdateModelsDefinition.extractWorkspace("/path/on/my/machine/voc-model/models/voc.uml");
		assertTrue(ex.equals("/path/on/my/machine"));
	}

}
