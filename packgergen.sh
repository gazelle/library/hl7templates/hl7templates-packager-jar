#!/bin/bash

skipUT="-DskipTests=false"

cd ../gocmodel-jar && mvn clean install $skipUT && cd ../hl7templates-model-jar && mvn clean install $skipUT && cd ../hl7templates-api && mvn clean install $skipUT && cd ../hl7templates-generator-jar && mvn clean install $skipUT && cd ../hl7templates-packager-jar && mvn clean install

hl7templates_packager_app_path="target/hl7templates-packager-app-`date +\"%Y-%m-%d-%H-%M-%S\"`"
mkdir $hl7templates_packager_app_path
cp -r target/appassembler/ $hl7templates_packager_app_path
mv $hl7templates_packager_app_path/appassembler $hl7templates_packager_app_path/packager
cd $hl7templates_packager_app_path
svn co https://scm.gforge.inria.fr/anonscm/svn/gazelle/validators/hl7templates/hl7templates-resources/trunk hl7templates-resources
cp ../classes/generateValidator.sh .
cp ../maven-archiver/pom.properties version.txt
sed -i 's/net.ihe.gazelle.hl7templates.packager.HL7Templates2GOC/-DHL7TEMP_RESOURCES_PATH="`pwd`\/..\/..\/hl7templates-resources\/" \\\nnet.ihe.gazelle.hl7templates.packager.HL7Templates2GOC/g' packager/bin/generator.sh
chmod +x packager/bin/generator.sh 
chmod +x generateValidator.sh
